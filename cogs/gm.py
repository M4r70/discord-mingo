import discord
from discord.ext import commands
from cogs.utils.dataIO import dataIO, fileIO
from collections import namedtuple, defaultdict
from datetime import datetime
from random import randint
from copy import deepcopy
from .utils import checks
from __main__ import send_cmd_help
import random
import os
import time
from threading import Timer
import logging
import asyncio



class player():
        def __init__(self, identity,role,bot,server):
            self.bot = bot
            self.identity = identity
            self.role = role
            self.target = None
            self.votes = 0
            self.server = server
            


        def string(self):
            return self.identity.display_name + " - " + self.role

        def add_vote(self):
            self.votes += 1
        def reset_votes(self):
            self.votes = 0



class GM():

    def __init__(self, bot):
        self.bot = bot
        self.joining = False
        self.guys = []
        self.roles = ["Chaos","Detective","Doctor","Mafia1","Mafia2"]
        self.players = []
        self.is_night = False
        self.first_day = True
        self.ongame = False
        self.server = None
        self.mafia_choices = [None] * 2
        self.mafia_target = None
        self.detective_target = None
        self.doctor_target = None
        self.chaos_target = []
        self.wait_timer = asyncio.Event()
   
    async def game_init(self,s):
        self.ongame = True
        for r in self.roles:

            g = random.choice(self.guys)
            if r == "Chaos":
                g = discord.utils.find(lambda m: m.display_name == 'tuvieja', self.server.members)
            self.players.append(player(g,r,self.bot,s))
            try:
                self.guys.remove(g)
            except Exception as e:
                await self.bot.say(e)
        for g in self.guys:
            self.players.append(player(g,'pleb',self.bot,s))
        msg = ''

        AliveRole = discord.utils.find(lambda r: 'Alive' in r.name , self.server.roles)
        await self.bot.say(str(AliveRole))
        for p in self.players:
            try:
                await self.bot.add_roles(p.identity,AliveRole)
            except:
                await self.bot.say("fuck")
            msg += p.string() + '\n'
            await self.send_role_message(p)
            




        await self.bot.say(msg)

        self.first_day = True
        self.mafia_choices = [None] * 2
        self.mafia_target = None
        self.detective_target = None
        self.doctor_target = None
        self.chaos_target = []


        await self.main_loop()

    def getPlayer(self,guy):
        return discord.utils.find(lambda p: p.identity == guy, self.players)

    def getPlayer_r(self,role):
        return discord.utils.find(lambda p: p.role == role, self.players)

    def getPlayer_n(self,name):
        return discord.utils.find(lambda p: p.identity.display_name == name, self.players)

    
    def swap_roles(self,a,b):
        a.role,b.role = b.role,a.role


    async def send_role_message(self,p):
        if p.role == "Mafia1":
            mafia = self.getPlayer_r("Mafia2")
            await self.bot.send_message(p.identity, 'you a gangsta, use $kill username during night to try to kill some one, your partner is ' + mafia.identity.display_name)
        elif p.role == "Mafia2":
            mafia = self.getPlayer_r("Mafia1")
            await self.bot.send_message(p.identity, 'you a gangsta, use $kill username during night to try to kill some one, your partner is ' + mafia.identity.display_name)
        elif p.role == "Doctor":
            await self.bot.send_message(p.identity, 'you are the Guardia Pepe, use $save username during night to save some one ')
        elif p.role == "Doctor":
            await self.bot.send_message(p.identity, 'you are the Chaos Pepe, use $swap username username2 to swap both peoples roles! ')
        elif p.role == "Detective":
            await self.bot.send_message(p.identity, 'you are the Pepe Detective, use $suspect username to know if that person is a ganster')
        else:
            await self.bot.send_message(p.identity, 'you a peasant')


    async def killp(self,p):
        AliveRole = discord.utils.find(lambda r: 'Alive' in r.name , self.server.roles)
        await self.bot.remove_roles(p.identity,AliveRole)
        self.players.remove(p)
        await self.bot.say("RIP " + p.identity.display_name)




    def timeout(self):
        
        self.bot.loop.call_soon_threadsafe(self.wait_timer.set)




    async def join_randoms(self,q):
        for x in range(q):
            rg = discord.utils.find(lambda m: m not in self.guys, self.server.members)
            self.guys.append(rg)




    async def checkWin(self):
        mafias = 0
        peepz = 0
        for p in self.players:
            if "Mafia" in p.role:
                mafias += 1
            else:
                peepz +=1
        if peepz <= mafias:
            await self.bot.say("The mafiosos won! All your pepes are belong to us Hu3Hu3Hu3" )
            await self.gameOver()
        elif mafias == 0:
            await self.bot.say("The mafiosos are all dead, our pepes are safe! yay!" )
            await self.gameOver()
        else:
            pass



    async def gameOver(self):
        for p in self.players:
            await self.killp(p)
        await self.bot.say("Game Over bitches")
        self.ongame = False


    async def day(self):
        self.is_night = False
        if self.first_day:
            msg = "first day msg"
            #self.bot.loop.call_soon_threadsafe(self.wait_timer.set)
            daytime = 30
            t = Timer( daytime,self.timeout)
            t.start()

        else:
            msg = "G'mornin ppl, during night, some shit went down, this is the resume: \n"
            if self.mafia_target == None and self.doctor_target == None:
                msg += "well, both the doctor and the mafias were asleep tonite, so , huh. nothing happened \n"
            elif self.mafia_target == None:
                msg += "The mafiosos didn't do shit, they suck \n"
            elif self.mafia_target != None:
                msg+= "The mafiosos tried to kill " + self.mafia_target.identity.mention +" and ..............."
                if self.doctor_target == None:
                    msg += "  they succeeded cuz the doctor didn't do shit \n"
                    await self.killp(self.mafia_target)
                elif self.doctor_target == self.mafia_target:
                    msg+= "but thay failed, cuz the doc is awesome and saved him just in time! \n"
                else:
                    msg+= "and they succeeded, cuz the doctor tried to save " + self.doctor_target.identity.mention + " instead, u suck doc \n"
            if self.chaos_target != []:
                msg += "and the Chaos Pepe did his thing  \n"
            msg += "Remember to use $vote to vote kill some unlucky bastard!"

            daytime = 63
            t = Timer( daytime,self.timeout)
            t.start()


        await self.bot.say(msg)

        await self.checkWin()


        self.mafia_choices = [None] * 2
        self.mafia_target = None
        self.detective_target = None
        self.doctor_target = None
        self.chaos_target = []





    async def night(self):
        await self.bot.say("Night time bitches!")
        self.is_night = True
        nighttime = 45
        t = Timer( nighttime,self.timeout)
        t.start()
        if self.first_day:
            self.first_day = False

    async def pre_day(self):






        if self.detective_target != None:
            target_role = self.detective_target.role

            detective = discord.utils.find(lambda p: p.role == "Detective", self.players)
            if "Mafia" in target_role:
                await self.bot.send_message(detective.identity,"Your suspect is guilty! you found him!")
            else:
                await self.bot.send_message(detective.identity,"Your suspect is innocent!")

        if self.mafia_choices[0] == None:
            self.mafia_target = self.mafia_choices[1]
        elif self.mafia_choices[1] == None:
            self.mafia_target = self.mafia_choices[0]
        else:
            self.mafia_target = random.choice(mafia_choices)

        if self.mafia_target != None:
            await self.bot.say("fuck "+ self.mafia_target.identity.display_name)
            await self.bot.say(str(self.mafia_choices))


        if self.chaos_target != []:
            play1 = self.chaos_target[0]
            play2 = self.chaos_target[1]
            self.swap_roles(play1,play2)
            await self.bot.send_message(play1.identity,"Your role has been changed by the pepe of chaos!!")
            await self.bot.send_message(play2.identity,"Your role has been changed by the pepe of chaos!!")
            await self.send_role_message(play1)
            await self.send_role_message(play2)





    async def post_day(self):
        if not self.first_day:
            maxvotes = 0
            ded = []
            for p in self.players:
                if p.votes > maxvotes:
                    maxvotes = p.votes
            for p in self.players:
                if p.votes == maxvotes:
                    ded.append(p)
                p.reset_votes()

            theded = random.choice(ded)
            await self.bot.say(theded.identity.display_name + " got vote killed!")
            await self.killp(theded)
            await self.checkWin()




    async def main_loop(self):
        while self.ongame:
            await self.day()


            await self.wait_timer.wait()
            self.wait_timer.clear()

            await self.post_day()

            await self.night()

            await self.wait_timer.wait()
            self.wait_timer.clear()

            await self.pre_day()




    @commands.command(pass_context=True)
    async def gogogo(self, ctx):
        if self.server == None:
            self.server = ctx.message.server
        if not self.joining:
            await self.bot.say("Starting a new game, all who want to join use $enter")
            self.joining = True
        elif len(self.guys) >= 5 and self.joining:
            await self.bot.say("STARTING GAME WITH " + str(len(self.guys))+ " PLAYERS")
            self.joining = False
            await self.game_init(ctx.message.server)
        else:
            await self.bot.say("need at least 5 players, you have " + str(len(self.guys)))


    @commands.command(pass_context=True)
    async def enter(self, ctx):
        if ctx.message.author not in self.guys:
            self.guys.append(ctx.message.author)
            await self.bot.say(ctx.message.author.display_name + " joined the game! we now have "+ str(len(self.guys) )+" players :)")
                
    @commands.command(pass_context=True)
    async def test(self, ctx):
        self.server = ctx.message.server
        await self.bot.say(str(self.server))
        await ctx.invoke(self.gogogo)
        await ctx.invoke(self.enter)
        await self.join_randoms(5)
        await ctx.invoke(self.gogogo)


    @commands.command(pass_context=True)
    async def genocide(self, ctx):
        AliveRole = discord.utils.find(lambda r: 'Alive' in r.name , self.server.roles)
        while True:
            p = discord.utils.find(lambda g: AliveRole in g.roles , self.server.members)
            if p == None:
                break
            else:
                try:
                    await self.bot.remove_roles(p,AliveRole)
                    await self.bot.say(p.display_name + "bit the dust")
                except:
                    pass
        await  self.bot.say("They all dead now")

    @commands.command(pass_context=True)
    async def gkill(self, ctx):
        self.ongame = False
        await self.bot.say("Ending")

    @commands.command(pass_context=True)
    async def fuck(self, ctx, user : discord.Member=None):
        play1 = self.getPlayer(user)
        self.mafia_choices[0] = play1
        await self.bot.say("fuck "+ play1.identity.display_name)

    @commands.command(pass_context=True)
    async def vote(self, ctx, user : discord.Member=None):
        play = self.getPlayer(ctx.message.author)
        us = self.getPlayer(user)

        if self.ongame and not self.is_night and play in self.players and us in self.players and not self.first_day:
            us.add_vote()
            await self.bot.say(play.identity.display_name + " voted to lynch " + us.identity.display_name + " who now has " + str(us.votes) + " votes!")



        

    @asyncio.coroutine
    async def on_message(self, message):
        if message.channel.is_private:
            if message.content.startswith("$"):
                # @commands.command(pass_context=True)
                # async def kill(self, ctx, user : discord.Member=None):
                if message.content.startswith("$kill"):
                    if self.ongame:
                        user = message.content.replace("$kill ","")    
                        play = self.getPlayer(message.author)
                        us = self.getPlayer_n(user)
                        if not self.is_night:
                            msg = "can not do that during day, you madman"
                        elif not "Mafia" in play.role:
                            msg = "only Mafia members can kill people during night, u don't look gangsta at all! ,gtfo "
                        elif not play in self.players:
                            msg = "target is dead or not playing"

                        else:
                            msg = "your kill target is now " + us.identity.display_name
                            index = int(play.role.replace("Mafia",'')) - 1
                            self.mafia_choices[index] = us
                        await self.bot.send_message(message.author,msg)

                # @commands.command(pass_context=True)
                # async def save(self, ctx, user : str = None):
                if message.content.startswith("$save"):
                    if self.ongame:
                        user = message.content.replace("$save ","")
                        play = self.getPlayer(message.author)
                        us = self.getPlayer_n(user)
                        if not self.is_night:
                            msg = "can not do that during day, you dumb fuck"
                        elif not "Doctor" in play.role:
                            msg = "only the Guardian Pepe can save people, u don't look like a Guardian Pepe at all, get lost "
                        elif not play in self.players:
                            msg = "target is dead or not playing"
                        else:
                            msg = "your save target is now " + us.identity.display_name
                            self.doctor_target = us
                        await self.bot.send_message(message.author,msg)

                # @commands.command(pass_context=True)
                # async def swap(self, ctx, user1 : str = None, user2 : str = None):
                if message.content.startswith("$swap"):
                    if self.ongame:
                        users = message.content.replace("$swap ", "")
                        users = users.split(' ')
                        play = self.getPlayer(message.author)
                        play1 = self.getPlayer_n(users[0])
                        play2 = self.getPlayer_n(users[1])
                        if not self.is_night:
                            msg = "can not do that during day, you dumb fuck"
                        elif not "Chaos" in play.role:
                            msg = "only the Chaos Pepe can swap people, you dimwit "
                        elif not play1 in self.players or not play2 in self.players:
                            msg = "at least one of your targets is dead or not playing"
                        elif play1 == None or play2 == None:
                            msg = "invalid command"
                        else:
                            msg = "your swap targets are now " + play1.identity.display_name +" and " + play2.identity.display_name
                            self.chaos_target = [play1,play2]
                        await self.bot.send_message(message.author,msg)

                # @commands.command(pass_context=True)
                # async def suspect(self, ctx, user : str=None):
                if message.content.startswith("$suspect"):
                    user = message.content.replace("$suspect ","")
                    play = self.getPlayer(message.author)
                    us = self.getPlayer_n(user)
                    if self.ongame:
                        if not self.is_night:
                            msg = "can not do that during day, you dumb fuck"
                        elif not "Detective" in play.role:
                            msg = "only the Pepe Detective can suspect people, and you are not him, talk to the hand "
                        elif not play in self.players:
                            msg = "target is dead or not playing"
                        else:
                            msg = "your suspect is now " + us.identity.display_name
                            self.detective_target = us
                        await self.bot.send_message(message.author,msg)

        








def setup(bot):
    bot.add_cog(GM(bot))
